install 
	git
	gitversion

	Chocolatey   -> The Package Manager for Windows 
	Homebrew pour windows


** La configuration de git

************   caommmande  ************

git config --list

*** Git global setup
    git config --global user.name "Elhadj DIALLO"
    git config --global user.email "elhadjdiallo.pro@gmail.com"


*** Create a new repository
    git clone git@gitlab.com:Elhadj-am/gittraining.git
    cd gittraining
    touch README.md
    git add README.md
    git commit -m "add README"
    git push -u origin master

*** Push an existing folder
    cd existing_folder
    git init
    git remote add origin git@gitlab.com:Elhadj-am/gittraining.git
    git add .
    git commit -m "Initial commit"
    git push -u origin master

*** Push an existing Git repository
    cd existing_repo
    git remote rename origin old-origin
    git remote add origin git@gitlab.com:Elhadj-am/gittraining.git
    git push -u origin --all
    git push -u origin --tags

**** les fichiers de config git

    cat /etc/gitconfig          -> cette congig s'applique a tous les user  de la machine
    cat ~/.gitconfig            -> cette congig s'applique a  un user donné
    cat <dir_repo>/.git/config  -> cette congig s'applique à un depot/user


************** Git log ******************

git log --graph --oneline
git log --graph --oneline --name-status

************** git revert ******************

Permet de revenir en arriere ( de un ou plusieur commits ).
C'est la maniere propre, car on garde une trace/historique  (a la difference de reset )

Etat final : commit1 -- commit2 -- commit3
Objectif : revenir a commit1

revert : commit1 -- commit2 -- commit3 -- commit2 -- commit1
reset:  commit1

git revert <num_dernier_commit> : revenir au dernier commit  ou encore
git revert HEAD~2..HEAD : Revenir de 2 commits en arriere

Ne pas oublie pusher le revert

************** git blame ******************

git blame : permet de lister les dernieres modifications de chaque ligne d'un fichier ( permet de voir qui a touché en dernier a quel fichier et a quelle date)
git blame <fichier>
git blame -L l1,l2 <fichier>   : de la ligne l1 à l2
git log -S "un mot" --pretty=format: '%h %an %ad %s'     : Recherche des logs concernant des lignes contenant "un mot"


************** Les branchs  ******************

Supprimer une branche locale
    git branch -d [nom-de-ma-branche]
    git branch -D [nom-de-ma-branche]    qui permet la suppression peut importe si elle a été mergé ou pas

Supprimer une branch distante sur git
    git push origin --delete [nom-de-ma-branche]


Supprimer un tag local
    git tag -d [nom-du-tag]

Supprimer un tag distant
git push [origin] --delete [nom-de-mon-tag]
